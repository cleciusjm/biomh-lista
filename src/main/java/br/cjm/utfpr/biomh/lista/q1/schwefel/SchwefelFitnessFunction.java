package br.cjm.utfpr.biomh.lista.q1.schwefel;

import br.cjm.utfpr.biomh.lista.q1.Q1FitnessFunction;

public class SchwefelFitnessFunction implements Q1FitnessFunction {
	private static final double D = 2.0;

	public double evaluate(double[] position) {
		double x = position[0];
		double y = position[1];
		double val = (D * 418.9829) - somat(1, D, x, y);
		return val;
	}

	private double somat(int init, double end, double x, double y) {
		double acc = 0.0;
		for (int i = init; i < end; i++) {
			acc += x * Math.sin(Math.sqrt(Math.abs(x)));
		}
		return acc;
	}

}
