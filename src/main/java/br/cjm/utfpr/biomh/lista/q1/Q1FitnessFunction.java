package br.cjm.utfpr.biomh.lista.q1;

@FunctionalInterface
public interface Q1FitnessFunction {
	double evaluate(double[] position);
}
