package br.cjm.utfpr.biomh.lista.q1;

import br.cjm.utfpr.biomh.lista.q1.f3.F3Runner;
import br.cjm.utfpr.biomh.lista.q1.rastrigin.RastringinRunner;
import br.cjm.utfpr.biomh.lista.q1.schwefel.SchewefelRunner;

public class Q1Main {

    public static void main(String[] args) {
	System.out.println("Problema 1 (Schewefel): ");
	new SchewefelRunner().call().forEach(System.out::println);
	System.out.println("Problema 2 (Rastringin): ");
	new RastringinRunner().call().forEach(System.out::println);
	System.out.println("Problema 3: ");
	new F3Runner().call().forEach(System.out::println);
    }

}
