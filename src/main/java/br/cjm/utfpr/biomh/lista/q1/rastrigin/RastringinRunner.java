package br.cjm.utfpr.biomh.lista.q1.rastrigin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import br.cjm.utfpr.biomh.lista.q1.Q1FinalSolution;
import br.cjm.utfpr.biomh.lista.q1.Q1Solution;
import br.cjm.utfpr.biomh.lista.q1.algorithm.ag.GeneticAlgorithm;
import br.cjm.utfpr.biomh.lista.q1.algorithm.clonalg.ClonalgAlgorithm;
import br.cjm.utfpr.biomh.lista.q1.algorithm.optainet.OptAiNetAlgorithm;
import br.cjm.utfpr.biomh.lista.q1.algorithm.pso.PSOAlgorithm;

public class RastringinRunner implements Callable<List<Q1FinalSolution>> {
    private static final int AGENTS = 500;
    private static final int ITERATIONS = 300;
    private static final double UPPER_BOUND = 5.0;
    private static final double LOWER_BOUND = -5.0;
    private static final int ENTITY_LENGTH = 2;

    public List<Q1FinalSolution> call() {
	List<Q1FinalSolution> results = new ArrayList<>();

	results.add(run(new GeneticAlgorithm(new RastriginFitnessFunction(), ITERATIONS, AGENTS, ENTITY_LENGTH, UPPER_BOUND, LOWER_BOUND)));
	results.add(run(new ClonalgAlgorithm(new RastriginFitnessFunction(), ITERATIONS, AGENTS, ENTITY_LENGTH, UPPER_BOUND, LOWER_BOUND)));
	results.add(
		run(new OptAiNetAlgorithm(new RastriginFitnessFunction(), ITERATIONS, AGENTS, ENTITY_LENGTH, UPPER_BOUND, LOWER_BOUND)));
	results.add(run(new PSOAlgorithm(new RastriginFitnessFunction(), ITERATIONS, AGENTS, ENTITY_LENGTH, UPPER_BOUND, LOWER_BOUND)));
	return results;
    }

    private Q1FinalSolution run(Callable<Q1Solution> algorithm) {
	try {
	    List<Q1Solution> solutions = new ArrayList<>();
	    for (int i = 0; i < 30; i++) {
		solutions.add(algorithm.call());
	    }
	    return new Q1FinalSolution(solutions, ITERATIONS);
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}
    }
}
